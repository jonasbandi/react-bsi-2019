import React, {lazy, Suspense} from 'react';
import './App.scss';
import {Route, BrowserRouter as Router, Switch, NavLink} from 'react-router-dom';
// import About from './about/About';
// import Home from './home/Home';

// Note: code-splitting with dynamic import() is optional. The components could also be imported statically.
const About = lazy(() => import('./about/About'));
const Home = lazy(() => import('./home/Home'));

function App() {
  return (
    <Suspense fallback={<h3>Loading ...</h3>}>
      <Router>
        <div className="App">
          <header className="App-header">
            <ul>
              <li>
                <NavLink to="/" exact>Home</NavLink>
              </li>
              <li>
                <NavLink to="/about" exact>About</NavLink>
              </li>
            </ul>
          </header>
          <div className="App-body">
            <Switch>
              <Route path="/about/:name?">
                <About/>
              </Route>
              <Route path="/">
                <Home/>
              </Route>
            </Switch>
          </div>
        </div>
      </Router>
    </Suspense>
  );
}

export default App;
