import React, {useState} from 'react';

export function Greeter() {

  const [name, setName] = useState('World');

  return (
    <div>
      <div>Hello {name}!</div>
      <input value={name} onChange={(e) => setName(e.target.value)} data-testid='name-input' />
    </div>
  )
}
