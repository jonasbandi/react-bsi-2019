import {observable, computed} from 'mobx';

interface ITodo {
    task: string;
    completed: boolean;
}

class TodoStore {
    @observable todos: ITodo[] = [];

    @computed get completedTodosCount() {
        return this.todos.filter(
            todo => todo.completed === true
        ).length;
    }

    @computed get report() {
        if (this.todos.length === 0)
            return "<none>";
        return `Next todo: "${this.todos[0].task}". ` +
            `Progress: ${this.completedTodosCount}/${this.todos.length}`;
    }

    addTodo(task: string) {
        this.todos.push({
            task: task,
            completed: false,
        });
    }

    removeTodo(index: number){
        this.todos.splice(index, 1);
    }
}

export {TodoStore};