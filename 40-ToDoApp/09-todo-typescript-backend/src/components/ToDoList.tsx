import * as React from 'react';
import ToDoListItem from './ToDoListItem';
import { IToDo } from '../model/ToDo';

type ToDoListProps = {
    todos: IToDo[],
    onRemoveToDo(todo: IToDo): void
}
function ToDoList({todos = [], onRemoveToDo} : ToDoListProps) {
  return (
    <ul id="todo-list" className="todo-list">
      {
        todos.map((t, index) => (
          <ToDoListItem key={index} todo={t} onRemoveToDo={onRemoveToDo}/>
        ))
      }
    </ul>
  );
}

export default ToDoList;
