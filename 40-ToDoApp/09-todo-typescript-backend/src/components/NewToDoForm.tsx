import * as React from 'react';

type NewToDoFormProps = {
    onAddToDo(title: string):void
}

export default class NewToDoForm extends React.Component<NewToDoFormProps> {

    state = {
        toDoTitle: ''
    };

	render() {
		return (
            <form className="new-todo" onSubmit={this.addToDo}>
                <input id="todo-text" name="toDoTitle" type="text" placeholder="What needs to be done?"
                       autoFocus={true}
                       autoComplete="off"
                       value={this.state.toDoTitle}
                       onChange={this.formChange}
                />
                <button id="add-button" className="add-button">+</button>
            </form>
		);
	}

    formChange = (e: React.FormEvent<HTMLInputElement>) => {
        this.setState({[e.currentTarget.name]: e.currentTarget.value});
    };

	addToDo = (e: React.FormEvent<HTMLFormElement>) => {
	    e.preventDefault();
        this.props.onAddToDo(this.state.toDoTitle);
        this.setState({ toDoTitle: ''});
    }
}
