export interface IToDo {
    id: number | undefined;
    title: string;
    completed: boolean;
}