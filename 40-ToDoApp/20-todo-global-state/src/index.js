import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

const store = {
  todos: [
    {id: 1, title: 'Learn React'},
    {id: 2, title: 'Learn Redux'}
  ],
  addToDo(title) {
    this.todos.push({id: Math.random(), title: title});
  },
  removeToDo(toDo) {
    this.todos.splice(this.todos.indexOf(toDo), 1);
  }
};

//window.store = store;

ReactDOM.render(<App store={store} />, document.getElementById('root'));
