import React from 'react';
import {HashRouter as Router, NavLink, Route, Switch} from 'react-router-dom';
import ErrorBoundary from './ErrorBoundary';
import ToDoScreen from './components/ToDoScreen';
import DoneScreen from './components/DoneScreen';

function App() {
  return (
    <div className="App">

      <div className="todoapp-header">
        <h1 id="title">Simplistic ToDo</h1>
        <h4>A most simplistic ToDo List in React.</h4>
      </div>

      <section className="todoapp">
        <Router>
          <ErrorBoundary>
            <div>
              <div className="nav">
                <NavLink exact to="/" activeClassName="selected">Pending</NavLink>
                <NavLink exact to="/done" activeClassName="selected">Done</NavLink>
              </div>
              <Switch>
                <Route path="/done"> <DoneScreen/> </Route>
                <Route path="/"> <ToDoScreen/> </Route>
              </Switch>
            </div>
          </ErrorBoundary>
        </Router>

      </section>
      <footer className="info">
        <p>JavaScript Example / Initial template from <a
          href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
        </p>
      </footer>
    </div>
  );
}

export default App;
