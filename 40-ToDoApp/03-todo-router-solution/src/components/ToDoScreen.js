import React, {useState, useEffect} from 'react';
import NewToDoForm from './NewToDoForm';
import ToDoList from './ToDoList';
import {loadToDos, storeToDos} from '../persistence';


function ToDoScreen() {

  const [todos, setTodos] = useState([]);

  useEffect(() => {
    const todos = loadToDos();
    updateToDos(todos);
  }, []);


  function addToDo(title) {
    const todos = loadToDos();
    const newToDos = [...todos, {id: Math.random(), title: title, completed: false}];
    storeToDos(newToDos);

    updateToDos(newToDos);
  }

  function completeToDo(toDo) {
    const todos = loadToDos();
    const updatedToDos = todos.map(t => t.id !== toDo.id ? t : {...toDo, completed: true});
    storeToDos(updatedToDos);

    updateToDos(updatedToDos);
  }

  function updateToDos(updatedToDos) {
    const pendingToDos = updatedToDos.filter(t => !t.completed);
    setTodos(pendingToDos);
  }

  return (
    <div>
      <NewToDoForm onAddToDo={addToDo}/>
      <div className="main">
        <ToDoList todos={todos} onRemoveToDo={completeToDo}/>
      </div>
    </div>
  );
}

export default ToDoScreen;
