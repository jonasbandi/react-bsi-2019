import React from 'react';
import { Provider } from 'unstated';
import NewToDoForm from './components/NewToDoForm';
import ToDoList from './components/ToDoList';

function App() {
  return (
    <div className="App">

      <div className="todoapp-header">
        <h1 id="title">Simplistic ToDo</h1>
        <h4>A most simplistic ToDo List in React.</h4>
      </div>

      <Provider>
        <section className="todoapp">

          <NewToDoForm/>

          <div className="main">
            <ToDoList/>
          </div>

        </section>
      </Provider>,
      <footer className="info">
        <p>JavaScript Example / Initial template from <a
          href="https://github.com/tastejs/todomvc-app-template">todomvc-app-template</a>
        </p>
      </footer>
    </div>
  );
}

export default App;
