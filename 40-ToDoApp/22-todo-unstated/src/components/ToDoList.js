import React from 'react';
import {PropTypes} from 'prop-types';
import ToDoListItem from './ToDoListItem';
import {connectToStore} from '../storeUtil';
import {ToDoStore} from '../ToDoStore';

const ToDoList = ({store}) => (
    <ul id="todo-list" className="todo-list">
        {
            store.state.todos.map(t => (
                <ToDoListItem key={t.id} todo={t} onRemoveToDo={store.removeToDo}/>
            ))
        }
    </ul>
);

// export default ToDoList;
export default connectToStore([ToDoStore])(ToDoList);
