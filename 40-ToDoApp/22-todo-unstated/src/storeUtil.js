import React from 'react';
import {Subscribe} from 'unstated';

export function connectToStore(stores){
  return function (Component) {
    return class extends React.Component {
      render(){
        return (
          <Subscribe to={stores}>
            {store => (
              <Component store={store}></Component>
            )}
          </Subscribe>
        );
      }
    }
  }
}