import { createSlice } from 'redux-starter-kit'
import * as actions from './todoReducer';

const messageSlice = createSlice({
    slice: 'message',
    initialState: '',
    reducers: {
        showMessage(state, action) {
            return action.payload;
        }
    },
    extraReducers:{
        [actions.addTodo](){ return ''},
        [actions.removeTodo](){ return ''},
        [actions.loadedToDos](){ return ''},
    }
});

export const { showMessage } = messageSlice.actions;

export default messageSlice.reducer;
