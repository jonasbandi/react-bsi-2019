// import {ADD_TODO, REMOVE_TODO, LOADED_TODOS} from './todoReducer';
// import {loadedToDos, addTodo, removeTodo} from './todoReducer';
import * as actions from './todoReducer';
import * as api from '../api';
import {showMessage} from './notificationActions';

export function loadedToDos(todos){
    return {type: actions.loadedToDos.type, payload: todos};
}

export function addedToDo(newToDo) {
    return { type: actions.addTodo.type, payload: newToDo };
}

export function removedToDo(toDo) {
    return { type: actions.removeTodo.type, payload: toDo };
}

export function addToDo(newToDo) {
    return (dispatch) => {
        dispatch(showMessage('Saving ...'));
        api.saveToDo(newToDo)
            .then((todo) => dispatch(addedToDo(todo)));
    };
}

export function removeToDo(toDo) {
    return (dispatch) => {
        dispatch(showMessage('Deleting ...'));
        api.deleteToDo(toDo)
            .then(() => dispatch(removedToDo(toDo)));
    }
}

export function loadToDos() {
    return (dispatch) => {
        dispatch(showMessage('Loading ...'));
        api.loadToDos()
            .then((todos) => dispatch(loadedToDos(todos)));
    }
}
