import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import { configureStore, getDefaultMiddleware } from 'redux-starter-kit'
import logger from 'redux-logger'
// import thunk from 'redux-thunk'
import todos from './todoReducer';
import message from './notificationReducer';

const rootReducer = combineReducers({todos, message});

export const store = configureStore({
  reducer: rootReducer,
  middleware: [...getDefaultMiddleware(), logger]
}
);

