import { createSlice } from 'redux-starter-kit'

const todosSlice = createSlice({
  slice: 'todos',
  initialState: [],
  reducers: {
    loadedToDos(state, action) {
      const loadedToDos = action.payload;
      state.push(...loadedToDos);
    },
    addTodo(state, action) {
      const newToDo = action.payload;
      state.push(newToDo);
    },
    removeTodo(state, action) {
      const newToDo = action.payload;
      state.splice(state.indexOf(newToDo), 1);
    },
  }
});

export const { loadedToDos, addTodo, removeTodo } = todosSlice.actions;

export default todosSlice.reducer;
