import React from 'react';
import ToDoListItem from './ToDoListItem';
import TodoStore from '../TodoStore';

function ToDoList() {

  const todoStore = TodoStore.useContainer();

  return (
    <ul id="todo-list" className="todo-list">
      {
        todoStore.todos.map(t => (
          <ToDoListItem key={t.id} todo={t} onRemoveToDo={() => todoStore.removeToDo(t)}/>
        ))
      }
    </ul>
  );
}


export default ToDoList;
