import React from 'react';
import {PropTypes} from 'prop-types';

ToDoListItem.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired
  }).isRequired,
  onRemoveToDo: PropTypes.func.isRequired
};

function ToDoListItem ({todo, onRemoveToDo}){
    return (
    <li>
      {todo.title}
      <button onClick={() => onRemoveToDo(todo)}>X</button>
    </li>
  );
}



export default ToDoListItem;
