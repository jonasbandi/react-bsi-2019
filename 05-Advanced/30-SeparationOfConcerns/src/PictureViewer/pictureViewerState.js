
export const initialState = {imageUrl: undefined, loading: false}

export function stateReducer(state, action) {
  switch (action.type) {
    case 'LOADING_START':
      return {...state, loading: true};
    case 'LOADING_SUCCESS':
      return {...state, imageUrl: action.payload, loading: false};
    default:
      throw new Error();
  }
}
