import React from "react";
import Spinner from '../Spinner';
import Image from './Image';

function View({loading, imageUrl, onLoad}) {

  let content;
  if (loading) {
    content = <Spinner/>;
  }
  else if (imageUrl){
    content = <Image {...{imageUrl}}/>
  }

  return (
    <>
      <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
        <button onClick={onLoad} style={{margin: 20}}>Next Picture</button>
        {content}
      </div>
    </>
  )
}

export default View;
