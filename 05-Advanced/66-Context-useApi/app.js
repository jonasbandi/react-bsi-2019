const {useState, useContext} = React;

// See: https://medium.com/free-code-camp/why-you-should-choose-usestate-instead-of-usereducer-ffc80057f815

//////////////////////
// Application Logic
////////////////////
function useCounterApi() {
  let [state, setState] = useState({
    count: 0,
    updated: new Date(),
    message: 'Great demo!'
  });

  function increment() {
    setState(prevState => ({...prevState, count: prevState.count + 1}));
  }

  function reset() {
    setState(prevState => ({...prevState, count: 0}));
  }

  return {
    increment,
    reset,
    count: state.count,
    updated: state.updated,
    message: state.message,
  }
}

//////////////////////
// Components
////////////////////
const CountContext = React.createContext();
function App() {

  const counterApi = useCounterApi();

  return (
    <CountContext.Provider value={counterApi}>
      <CountScreen/>
    </CountContext.Provider>
  )
}

function CountScreen() {
  return (
    <div>
      <CountCard/>
    </div>
  )
}

function CountCard() {

  const {reset} = useContext(CountContext);

  return (
    <div>
      <CountDisplay/>

      <div>
        <button onClick={reset}>Reset</button>
      </div>
    </div>
  )
}

function CountDisplay() {

  const {increment, count} = useContext(CountContext);

  return (
    <div>
      Count: {count}
      <div>
        <button onClick={increment}>Increment</button>
      </div>
    </div>
  )
}

ReactDOM.render(<App/>, document.getElementById('root'));

